# KDE Apps website

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_apps-kde-org)](https://binary-factory.kde.org/job/Website_apps-kde-org/)

This is a Symfony web application displaying all the applications made by the KDE community.

https://community.kde.org/KDE.org/applications

## Development

```sh
git clone git@invent.kde.org:websites/kde-org-applications
cd kde-org-applications
git clone git@invent.kde.org:websites/kde-org-applications-extractor
cd kde-org-applications-extractor
bundle install
bundle exec ./appstream.rb
bundle exec ./appstream_mkindex.rb
cd ..
composer install
php -S localhost:8002 routing.php
```

Open http://localhost:8002

## Maintainer

Jonathan Riddell and Carl Schwan. Contact us in #kde-www or #kde-devel :)

## License

This project is licensed under AGPL 3 or later. Individual files in this projects can be licensed under another compatible license

Check `./COPYING` for more information.
