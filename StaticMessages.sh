#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-License-Identifier: CC0-1.0

FILENAME="kde-org-applications"

function export_pot_file # First parameter will be the path of the pot file we have to create, includes $FILENAME
{
    potfile=$1
    composer install -q
    php bin/console translation:update -q --output-format po --force en
    mv translations/messages+intl-icu.en.po "$potfile"
    python3 bin/cleanpot "$potfile"
}

function import_po_files # First parameter will be a path that will contain several .po files with the format LANG.po
{
    podir=$1
    mkdir -p translations
    for translation in $podir/*.po; do
        lang=`basename $translation`
        if [ "$lang" = "ca@valencia" ]; then
            lang="ca_valencia"
        fi
        msgattrib --translated --no-fuzzy --no-obsolete -o "translations/messages.${lang}" "$translation"
    done

    cp config/services.php.in config/services.php

    for translation in translations/messages.*.po; do
        # print the obsolete translations
        langpo=`basename $translation | sed -e 's,^messages\.,,g'`
        if [ ! -e "$podir/$langpo" ]; then
            echo "WARNING: $translation has no more backing po file in l10n"
        fi

        # generate data for language bar
        echo "$translation" | sed 's/[^\.]*\.\([^\.]*\)\.po$/"\1",/g' >> config/services.php
    done

    echo "]);" >> config/services.php

}
