<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Model;

/**
 * Enum AppDataType
 * @package App\Model
 */
abstract class AppDataType {
    const Application = 0;
    const Addon = 1;
    const Console = 2;
}
