<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Model;

const LANG = 'C';

class Screenshot
{
    private $thumbnail = null;
    private $sourceImage = null;
    private $default = null;
    private $caption = null;

    public function __construct(string $thumbnail, string $sourceImage, string $default)
    {
        $this->thumbnail = $thumbnail;
        $this->sourceImage = $sourceImage;
        $this->default = $default;
    }

    public static function fromData(array $screenshot): ?Screenshot
    {
        if (!in_array('lang', $screenshot['source-image'])
            || $screenshot['source-image']['lang'] === LANG) {
            $screenshotObject = new Screenshot('/applications'.$screenshot['thumbnails'][0]['url'], $screenshot['source-image']['url'], isset($screenshot['default']) && $screenshot['default'] == true);

            if (isset($screenshot['caption'])) {
                $screenshotObject->setCaption($screenshot['caption']);
            }
            return $screenshotObject;
        }
        return null;
    }

    /**
     * @return array?
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @param array $caption
     */
    public function setCaption(array $caption): void
    {
        $this->caption = $caption;
    }

    /**
     * @return string|null
     */
    public function getSourceImage(): ?string
    {
        return $this->sourceImage;
    }

    /**
     * @return string|null
     */
    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    /**
     * @return string|null
     */
    public function getDefault(): ?string
    {
        return $this->default;
    }

}
